package model;

public class Configuration {

	/// Stringa che indica se si sta eseguendo il programma per combinare o per comporre.
	private String executionMode;
	/// Root path
	private String rootPath;
	/// Stringa contenente il path del dataset
	private String datasetPath;
	/// Stringa contenente il nome del file che contiene le directory dove si trovano le visualFeature da usare
	private String visualFeatureFileName;
	/// Stringa contenente la directory dove si trovano le visualFeature da usare
	private String visualFeatureDir;
	/// Stringa che contiene il nome del file dove si trovano i mapping delle categorie da usare
	private String categoryIdMappingFileName;
	/// Stringa che indica la directory dove si troverà tutto quello che concerne la parte di allineamento
	private String alignmentDatasetPartPath;
	/// Stringa contenente la prima parte del nome dei file dove sono contenuti gli id delle immagini che compongono il dataset
	private String imageIdListFirstPartFileName;
	/// Stringa contenente il nome del file che contiene le informazioni delle categorie usate
	private String usedCategoryFileName;
	/// Stringa contenente il path dove si trovano le immagini delle instance
	private String instanceImagePartPath;
	/// Stringa contenente la directory dove si trovano i dizionari
	private String dictionariesDir;
	
	
	/*
	 * Metodi getter e setter per i diversi attributi
	 */
	public String getRootPath() {
		return rootPath;
	}
	public void setRootPath(String rootPath) {
		this.rootPath = rootPath;
	}
	public String getDatasetPath() {
		return datasetPath;
	}
	public void setDatasetPath(String datasetPath) {
		this.datasetPath = datasetPath;
	}
	public String getAlignmentDatasetPartPath() {
		return alignmentDatasetPartPath;
	}
	public void setAlignmentDatasetPartPath(String alignmentDatasetPartPath) {
		this.alignmentDatasetPartPath = alignmentDatasetPartPath;
	}
	public String getImageIdListFirstPartFileName() {
		return imageIdListFirstPartFileName;
	}
	public void setImageIdListFirstPartFileName(
			String imageIdListFirstPartFileName) {
		this.imageIdListFirstPartFileName = imageIdListFirstPartFileName;
	}
	public String getUsedCategoryFileName() {
		return usedCategoryFileName;
	}
	public void setUsedCategoryFileName(String usedCategoryFileName) {
		this.usedCategoryFileName = usedCategoryFileName;
	}
	public String getInstanceImagePartPath() {
		return instanceImagePartPath;
	}
	public void setInstanceImagePartPath(String instanceImagePath) {
		this.instanceImagePartPath = instanceImagePath;
	}
	public String getDictionariesDir() {
		return dictionariesDir;
	}
	public void setDictionariesDir(String dictionariesDir) {
		this.dictionariesDir = dictionariesDir;
	}
	public String getVisualFeatureFileName() {
		return visualFeatureFileName;
	}
	public void setVisualFeatureFileName(String visualFeatureFileName) {
		this.visualFeatureFileName = visualFeatureFileName;
	}
	public String getVisualFeatureDir() {
		return visualFeatureDir;
	}
	public void setVisualFeatureDir(String visualFeatureDir) {
		this.visualFeatureDir = visualFeatureDir;
	}
	public String getCategoryIdMappingFileName() {
		return categoryIdMappingFileName;
	}
	public void setCategoryIdMappingFileName(String categoryIdMappingFileName) {
		this.categoryIdMappingFileName = categoryIdMappingFileName;
	}
	public String getExecutionMode() {
		return executionMode;
	}
	public void setExecutionMode(String executionMode) {
		this.executionMode = executionMode;
	}
	
	
}
