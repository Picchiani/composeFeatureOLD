package model;

public class FeatureLSABoN extends TextFeature {
	public FeatureLSABoN() {
		super.setFeatureType(FeatureType.LSABoN);
	}
	
	public FeatureLSABoN(int dictSize) {
		super.setFeatureType(FeatureType.LSABoN);
		super.setDictionarySize(dictSize);
	}

	@Override
	public void printFeature() {
		System.out.println(this.getFeatureType());		
	}

	@Override
	public String featurePath() {
		return "LSAbagOfN.dat";
	}

	@Override
	public String getInfo() {
		String ret = super.getFeatureType() +" " +super.getDictionarySize();
		return ret;
	}

	@Override
	public String getDirNamePart() {
		String ret = new String();
		ret += super.getFeatureType(); 
		return ret;
	}
}
