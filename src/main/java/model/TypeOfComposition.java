package model;

public enum TypeOfComposition {
	addBalanced, addAllNegative, oneFile;
}
