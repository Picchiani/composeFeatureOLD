package model;

public class FeaturenLSABoN extends TextFeature {
	public FeaturenLSABoN() {
		super.setFeatureType(FeatureType.nLSABoN);
	}
	
	public FeaturenLSABoN(int dictSize) {
		super.setFeatureType(FeatureType.nLSABoN);
		super.setDictionarySize(dictSize);
	}

	@Override
	public void printFeature() {
		System.out.println(this.getFeatureType());		
	}

	@Override
	public String featurePath() {
		return "NormalizedLSAbagOfN.dat";
	}

	@Override
	public String getInfo() {
		String ret = super.getFeatureType() +" " +super.getDictionarySize();
		return ret;
	}

	@Override
	public String getDirNamePart() {
		String ret = new String();
		ret += super.getFeatureType(); 
		return ret;
	}
}
