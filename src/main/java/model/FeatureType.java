package model;

public enum FeatureType {
	HOG, SIFT, LBP, BoN, BoNA, BoNAV, LSABoN, LSABoNA, LSABoNAV, nLSABoN; 
}
