package model;

public class FeatureHOG extends VisualFeature {

	public FeatureHOG(int width, int height) {
		super.setFeatureType(FeatureType.HOG);
		super.setWidth(width);
		super.setHeight(height);
	}

	@Override
	public void printFeature() {
		System.out.println(this.getFeatureType());		
	}

	@Override
	public String featurePath() {
		return super.getFeatureType() +"/" +super.getWidth() +"_" +super.getHeight() +"/";
	}

	@Override
	public String getInfo() {
		String ret = super.getFeatureType() +" " +super.getWidth() +"_" +super.getHeight() +" ";
		return ret;
	}

	@Override
	public String getDirNamePart() {
		String ret = new String();
		ret += super.getFeatureType(); 
		return ret;
	}
}
