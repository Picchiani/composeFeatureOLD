package model;

public class FeatureLSABoNA extends TextFeature {
	public FeatureLSABoNA() {
		super.setFeatureType(FeatureType.LSABoNA);
	}
	
	public FeatureLSABoNA(int dictSize) {
		super.setFeatureType(FeatureType.LSABoNA);
		super.setDictionarySize(dictSize);
	}

	@Override
	public void printFeature() {
		System.out.println(this.getFeatureType());		
	}

	@Override
	public String featurePath() {
		return "LSAbagOfNA.dat";
	}

	@Override
	public String getInfo() {
		String ret = super.getFeatureType() +" " +super.getDictionarySize();
		return ret;
	}

	@Override
	public String getDirNamePart() {
		String ret = new String();
		ret += super.getFeatureType(); 
		return ret;
	}
}
