package model;

public class FeatureBoNA extends TextFeature {

	public FeatureBoNA() {
		super.setFeatureType(FeatureType.BoNA);
	}
	
	public FeatureBoNA(int dictSize) {
		super.setFeatureType(FeatureType.BoNA);
		super.setDictionarySize(dictSize);
	}

	@Override
	public void printFeature() {
		System.out.println(this.getFeatureType());		
	}

	@Override
	public String featurePath() {
		return "bagOfNA.dat";
	}

	@Override
	public String getInfo() {
		String ret = super.getFeatureType() +" " +super.getDictionarySize();
		return ret;
	}

	@Override
	public String getDirNamePart() {
		String ret = new String();
		ret += super.getFeatureType(); 
		return ret;
	}

}
