package model;

public abstract class VisualFeature extends Feature {
	private int width;
	private int height;
	
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	
	@Override
	public String featurePath() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDirNamePart() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void printFeature() {
		// TODO Auto-generated method stub

	}

}
