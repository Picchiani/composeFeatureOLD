package model;

public class FeatureLSABoNAV extends TextFeature {
	public FeatureLSABoNAV() {
		super.setFeatureType(FeatureType.LSABoNAV);
	}
	
	public FeatureLSABoNAV(int dictSize) {
		super.setFeatureType(FeatureType.LSABoNAV);
		super.setDictionarySize(dictSize);
	}

	@Override
	public void printFeature() {
		System.out.println(this.getFeatureType());		
	}

	@Override
	public String featurePath() {
		return "LSAbagOfNAV.dat";
	}

	@Override
	public String getInfo() {
		String ret = super.getFeatureType() +" " +super.getDictionarySize();
		return ret;
	}

	@Override
	public String getDirNamePart() {
		String ret = new String();
		ret += super.getFeatureType(); 
		return ret;
	}
}
