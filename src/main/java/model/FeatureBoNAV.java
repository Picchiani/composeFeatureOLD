package model;

public class FeatureBoNAV extends TextFeature {
	public FeatureBoNAV() {
		super.setFeatureType(FeatureType.BoNAV);
	}
	
	public FeatureBoNAV(int dictSize) {
		super.setFeatureType(FeatureType.BoNAV);
		super.setDictionarySize(dictSize);
	}

	@Override
	public void printFeature() {
		System.out.println(this.getFeatureType());		
	}

	@Override
	public String featurePath() {
		return "bagOfNAV.dat";
	}

	@Override
	public String getInfo() {
		String ret = super.getFeatureType() +" " +super.getDictionarySize();
		return ret;
	}

	@Override
	public String getDirNamePart() {
		String ret = new String();
		ret += super.getFeatureType(); 
		return ret;
	}
}
