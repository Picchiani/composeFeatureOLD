package model;

public abstract class Feature {
	private FeatureType featureType;

	public FeatureType getFeatureType() {
		return featureType;
	}

	public void setFeatureType(FeatureType featureType) {
		this.featureType = featureType;
	}
	
	/**
	 * Costruisce il path dove trovare i file json dell data feature partendo dai sui dati
	 */
	public abstract String featurePath();
	
	/**
	 * Costruisce una stringa contenente le informazioni della feature
	 * @return
	 */
	public abstract String getInfo();
	
	/**
	 * Ritorna una stringa che serve per fare una parte della directory dove trovare i file
	 * @return
	 */
	public abstract String getDirNamePart();
	
	public abstract void printFeature();

}
