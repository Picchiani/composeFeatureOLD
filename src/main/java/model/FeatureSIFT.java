package model;

public class FeatureSIFT extends VisualFeature {
	private int dictionarySize;

	public FeatureSIFT(int width, int height, int dictSize) {
		super.setFeatureType(FeatureType.SIFT);
		super.setWidth(width);
		super.setHeight(height);
		this.dictionarySize = dictSize;
	}

	public int getDictionarySize() {
		return dictionarySize;
	}

	public void setDictionarySize(int dictionarySize) {
		this.dictionarySize = dictionarySize;
	}

	@Override
	public void printFeature() {
		System.out.println(this.getFeatureType() +" dictionary size: " +dictionarySize);
		
	}
	
	@Override
	public String featurePath() {
		return super.getFeatureType() +"/" +getDictionarySize() +"/" +super.getWidth() +"_" +super.getHeight() +"/";
	}
	
	@Override
	public String getInfo() {
		String ret = super.getFeatureType() +" " +super.getWidth() +"_" +super.getHeight() +" " +dictionarySize +" ";
		return ret;
	}
	
	@Override
	public String getDirNamePart() {
		String ret = new String();
		ret += super.getFeatureType();
		ret += dictionarySize; 
		return ret;
	}
}
