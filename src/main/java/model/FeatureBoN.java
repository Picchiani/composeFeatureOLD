package model;

public class FeatureBoN extends TextFeature {

	public FeatureBoN() {
		super.setFeatureType(FeatureType.BoN);
	}
	
	public FeatureBoN(int dictSize) {
		super.setFeatureType(FeatureType.BoN);
		super.setDictionarySize(dictSize);
	}

	@Override
	public void printFeature() {
		System.out.println(this.getFeatureType());		
	}

	@Override
	public String featurePath() {
		return "bagOfN.dat";
	}

	@Override
	public String getInfo() {
		String ret = super.getFeatureType() +" " +super.getDictionarySize();
		return ret;
	}

	@Override
	public String getDirNamePart() {
		String ret = new String();
		ret += super.getFeatureType(); 
		return ret;
	}

}
