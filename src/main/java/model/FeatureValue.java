package model;

import java.math.BigDecimal;

public class FeatureValue {

	private int pos;
	private BigDecimal value;
	
	public FeatureValue(int pos, BigDecimal value) {
		this.pos = pos;
		this.value = value;
	}
	
	public int getPos() {
		return pos;
	}
	
	public void incrementPos(int increment) {
		pos += increment;
	}

	

	public void setPos(int pos) {
		this.pos = pos;
	}
	
	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		FeatureValue cloned = (FeatureValue) super.clone();
		cloned.setPos(pos);
		cloned.setValue(value);
		return cloned;
	}
	
	
}
