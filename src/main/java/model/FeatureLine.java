package model;

import java.util.Vector;

public class FeatureLine {
	String imageId;
	String instanceId;
	Vector<FeatureValue> featureValues;
	
	public FeatureLine(String imageId, String instanceId, Vector<FeatureValue> featureValues) {
		this.imageId = imageId;
		this.instanceId = instanceId;
		this.featureValues = featureValues;
	}

	public FeatureLine(String imageId, Vector<FeatureValue> featureValues) {
		this.imageId = imageId;
		this.featureValues = featureValues;	
	}

	public String getImageId() {
		return imageId;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public Vector<FeatureValue> getFeatureValues() {
		return featureValues;
	}
}
