package model;

public class FeatureLBP extends VisualFeature {
	boolean normalized;
	
	public FeatureLBP(int width, int height, boolean normalized) {
		super.setFeatureType(FeatureType.LBP);
		super.setWidth(width);
		super.setHeight(height);
		this.normalized = normalized;
	}

	@Override
	public void printFeature() {
		System.out.println(this.getFeatureType());				
	}
	
	@Override
	public String featurePath() {
		String featurePath;
		if(normalized) {
			featurePath = "n" +super.getFeatureType() +"/" +super.getWidth() +"_" +super.getHeight() +"/";
		} else {
			featurePath = super.getFeatureType() +"/" +super.getWidth() +"_" +super.getHeight() +"/";
		}
		return featurePath;
	}
	
	@Override
	public String getInfo() {
		String info;
		if(normalized) {
			info = "n" +super.getFeatureType() +" " +super.getWidth() +"_" +super.getHeight() +" ";
		} else {
			info = super.getFeatureType() +" " +super.getWidth() +"_" +super.getHeight() +" ";
		}
		return info;
	}
	
	@Override
	public String getDirNamePart() {
		String ret = new String();
		if(normalized) {
			ret += "n" +super.getFeatureType();
		} else {
			ret += super.getFeatureType();
		}
		return ret;
	}
}
