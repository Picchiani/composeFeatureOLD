package main;

import java.util.Vector;

import controller.CategoryCompositionManager;
import controller.CombineFeatureVectorManager;
import controller.FileManager;
import controller.JsonCombinationsManager;
import controller.PropertiesManager;
import model.Configuration;
import model.Feature;
import model.FeatureHOG;
import model.FeatureSIFT;
import model.TypeOfComposition;

public class Main {
	
	/// Stringa che indica il path del file di properties
	private static String propertiesFilePath = "configuration.properties";

	/// Stringa contenente il path assoluto del dataset
	public static String datasetAbsolutePath;
	/// Stringa contenente il file dove si trovano i path delle diverse feature di cui creare l'unico file per l'addestramento con liblinear
	public static String visualFeatureListPath;	
	/// Stringa contenente la directory delle feature visuali
	public static String visualFeatureDir;
	/// Stringa che indica il path del file che contiene il mapping delle categorie da seguire
	public static String categoryIdMappingPath;
	/// Stringa che indica se il programma si sta eseguendo per combinare feature (combine) o per comporre i file delle categorie (compose)
	public static String executionMode;

	/// Stringa che indica la phase per cui si stanno costruendo le combinazioni
	public static String phase = "val";
	/// Enumerazione che indica il tipo di combinazione da effettuare
	public static TypeOfComposition typeOfComposition = TypeOfComposition.oneFile;

	public static Vector<Vector<Feature>> combinations = new Vector<Vector<Feature>>();
	public static CombineFeatureVectorManager combineFeatureVectorManager = new CombineFeatureVectorManager();
	public static JsonCombinationsManager jsonCombinationsManager = new JsonCombinationsManager();
	public static CategoryCompositionManager categoryCompositionManager;

	public static FileManager fileManager = new FileManager();
	
	/// Oggetto che contiene i diversi valori di configurazione
	public static Configuration configuration;
	/// Variabile booleana che indica se si vogliono creare dei file che contengono solo la feature LSABoN normalizzata
	public static boolean onlynLSABoN = false;
	/// Indica se si vuole creare un file per il test set con un numero limitato di instance per categoria
	public static boolean createLimitatedTestSet = false;
	/// Indica se si vuole usare il codice per unire le feature visuali di train e val per formare trainval
	public static boolean createTrainvalVisualFeature = true;
	
	public static String composition(String fileDir, String phase, TypeOfComposition typeOfComposition) {
		System.out.println("Scrittura " +phase +".dat per " +fileDir);
		String writeFilePath = null;
		
		if(typeOfComposition.equals(TypeOfComposition.addAllNegative)) {
			// TODO inserire metodo
		} else if(typeOfComposition.equals(TypeOfComposition.addBalanced)) {
			writeFilePath = categoryCompositionManager.addNegativeExample(fileDir, phase);
		} else if(typeOfComposition.equals(TypeOfComposition.oneFile)) {
			writeFilePath = categoryCompositionManager.writeOneFile(fileDir, phase);
		}
		
		return writeFilePath;
			
	}

	public static void main(String[] args) {
		
		PropertiesManager propManager = new PropertiesManager();
		
		/// Leggo dal file properties i diversi parametri di configurazione
		configuration = propManager.readConfigurationFromPropertiesFile(propertiesFilePath);
		executionMode = configuration.getExecutionMode();
		/// Con i valori letti costruisco i path assoluti
		datasetAbsolutePath = configuration.getRootPath() +configuration.getDatasetPath();
		visualFeatureListPath = configuration.getRootPath() +configuration.getDatasetPath() +configuration.getVisualFeatureFileName();
		visualFeatureDir = configuration.getRootPath() +configuration.getDatasetPath() +configuration.getVisualFeatureDir();
		categoryIdMappingPath = configuration.getRootPath() +configuration.getDatasetPath() +configuration.getCategoryIdMappingFileName();

		categoryCompositionManager = new CategoryCompositionManager();
		
		/// Parte di codice per creare un testset con un numero limitato di instance per categoria
		if(createLimitatedTestSet) {
			/// Leggo il file che contiene i path da comporre
			Vector<String> trainFileDirs = fileManager.readFile(visualFeatureListPath);
			
			phase = "test";
			for(String trainFileDir : trainFileDirs) {
				trainFileDir = trainFileDir.replace("train", phase);
				categoryCompositionManager.writeTestSetWithXinstanceForCategory(trainFileDir, phase, 200);
			}
			return;
		}
		
		/// Parte di codice per unire le feature visuali di train e val per formare trainval
		if(createTrainvalVisualFeature) {
			System.out.println("Composizione dei file delle visualFeature di train e val in trainval...");
			Vector<String> trainFileDirs = fileManager.readFile(visualFeatureListPath);
			for(String trainFileDir : trainFileDirs) {
				categoryCompositionManager.writeTrainvalFile(trainFileDir);
			}
			System.out.println("Finita composizione.");
			return;
		}
		
		/// Parte di codice che serve per creare i file con le feature del solo NormalizedLSABoN
		if(onlynLSABoN) {
			Main.phase = "val";
			combineFeatureVectorManager.createDatasetFileOnlyFornLSABoN(datasetAbsolutePath +Main.phase +"InstanceList");
			Main.phase = "train";
			combineFeatureVectorManager.createDatasetFileOnlyFornLSABoN(datasetAbsolutePath +Main.phase +"InstanceList");
			Main.phase = "test";
			combineFeatureVectorManager.createDatasetFileOnlyFornLSABoN(datasetAbsolutePath +Main.phase +"InstanceList");
		} else {


			if(executionMode.equals("compose")) {

				/// Leggo il file che contiene i path da comporre
				Vector<String> trainFileDirs = fileManager.readFile(visualFeatureListPath);

				for(String trainFileDir : trainFileDirs) {
					//				phase = "train";
					//				composition(trainFileDir, phase, typeOfComposition);

					//				phase = "val";
					//				trainFileDir = trainFileDir.replace("train", phase);
					//				composition(trainFileDir, phase, typeOfComposition);
					//				trainFileDir = trainFileDir.replace("val", "train");

					phase = "trainval";
					System.out.println("Scrittura " +phase +".dat");
					//				categoryCompositionManager.writeTrainvalFile(trainFileDir);
					trainFileDir = trainFileDir.replace("train", phase);
					composition(trainFileDir, phase, typeOfComposition);

					//				phase = "test";
					//				trainFileDir = trainFileDir.replace("trainval", phase);
					//				composition(trainFileDir, phase, typeOfComposition);

				}
			}

			else {

				Vector<Vector<Feature>> combinations = new Vector<Vector<Feature>>();
				Vector<Feature> combination = new Vector<Feature>();
				combination.add(new FeatureHOG(128, 96));
				//			combination.add(new FeatureSIFT(128, 96, 500));
				combinations.add(combination);
				//			combination = new Vector<Feature>();
				//			combination.add(new FeatureHOG(128, 112));
				//			combination.add(new FeatureSIFT(128, 112, 200));
				//			combinations.add(combination);

				long startTime = System.currentTimeMillis();

//				phase = "train";
//				combineFeatureVectorManager.composeVisualFeatureVector(combinations);
//
//				long stopTime = System.currentTimeMillis();
//				long elapsedTime = stopTime - startTime;
//				FileManager.writeExecutionTime("Tempo impiegato per combinazioni " +phase +": " +elapsedTime);

				startTime = System.currentTimeMillis();

				phase = "trainval";
				combineFeatureVectorManager.combineFeature(combinations);

				long stopTime = System.currentTimeMillis();
				long elapsedTime = stopTime - startTime;
				FileManager.writeExecutionTime("Tempo impiegato per combinazioni " +phase +": " +elapsedTime);

/*			
			startTime = System.currentTimeMillis();

			phase = "val";
			composeFeatureVectorManager.composeVisualFeatureVector(combinations);

			stopTime = System.currentTimeMillis();
			elapsedTime = stopTime - startTime;
			FileManager.writeExecutionTime("Tempo impiegato per combinazioni " +phase +": " +elapsedTime);

			startTime = System.currentTimeMillis();

			phase = "test";
//			composeFeatureVectorManager.composeVisualFeatureVector(combinations);
			composeFeatureVectorManager.combineFeature(combinations);

			long stopTime = System.currentTimeMillis();
			long elapsedTime = stopTime - startTime;
			FileManager.writeExecutionTime("Tempo impiegato per combinazioni " +phase +": " +elapsedTime);
				 */					
			}

		}

	}

}
