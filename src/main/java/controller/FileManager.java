package controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

import main.Main;

public class FileManager {

	/**
	 * Prende i file presenti nella directory passata e li ritorna in un array di File
	 * @param dirPath String che indica il path assoluto della directory di cui si vogliono prendere i file
	 * @return File[] array contenente i file
	 */
	public static File[] getFiles(String dirPath) {

		/* Get the directory */
		File dir = new File(dirPath);

		/* This filter only returns files */
		FileFilter fileFilter = new FileFilter() {
			public boolean accept(File file) {
				return file.isFile();
			}
		};

		/* Returns the list of file of dir */
		return dir.listFiles(fileFilter);

	}
	
	/**
	 * Prende in ingresso un file in cui ogni linea rappresenta un path e quindi inserisce ogni linea in un vettore di stringhe.
	 * @param filePath String path assoluto del file da leggere linea per linea
	 * @return Vector<String> che contiene i path estratti dal file del percorso passato.
	 */
	public Vector<String> readFile(String filePath) {
		Vector<String> retListOfPaths = new Vector<String>();
		
		try {
			String line;
			BufferedReader buffReader = new BufferedReader(new FileReader(filePath));
			
			while(true) {
				line = buffReader.readLine();
				if(line == null)
					break;
				
				retListOfPaths.add(line);
			}
			buffReader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return retListOfPaths;
	}
	
	/**
	 * Metodo che apre un file in modalità append e vi aggiunge la stringa passata.
	 * @param modelPath String del path del modello che si vuole aggiungere al file.
	 */
	public static void writeCombinationPathToFile(String combinationPath, String combinationName) {
		try {
			BufferedWriter buffWriter = new BufferedWriter(new FileWriter(Main.phase +"PathList" +combinationName  +".txt", true));
			buffWriter.write(combinationPath+"\n");
			buffWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void writeFilePath(String filePath, String phase) {
		try {
			BufferedWriter buffWriter = new BufferedWriter(new FileWriter(phase +"PathList.txt", true));
			buffWriter.write(filePath+"\n");
			buffWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Scrive su un file executionTime.txt situato nel path dell'applicazione la stringa passatagli
	 * @param executionTimeString String da scrivere sul file
	 */
	public static void writeExecutionTime(String executionTimeString) {
		try {
			BufferedWriter buffWriter = new BufferedWriter(new FileWriter("executionTime.txt", true));
			buffWriter.write(executionTimeString +"\n");
			buffWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
