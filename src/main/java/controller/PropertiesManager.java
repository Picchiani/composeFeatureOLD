package controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import model.Configuration;

public class PropertiesManager {

	public PropertiesManager() {
		
	}
	
	public Configuration readConfigurationFromPropertiesFile(String propertiesFilePath) {
		
		Properties prop = new Properties();
		
		Configuration configurationRet = new Configuration();
		
		InputStream input = null;
		 
		try {
	 
			input = new FileInputStream(propertiesFilePath);
	 
			/// Carico il file di properties
			prop.load(input);
	 
			/// Prendo i valori e li inserisco nel Configuration da ritornare
			configurationRet.setExecutionMode(prop.getProperty("executionMode"));
			configurationRet.setRootPath(prop.getProperty("rootPath"));
			configurationRet.setDatasetPath(prop.getProperty("datasetPath"));
			configurationRet.setVisualFeatureFileName(prop.getProperty("visualFeatureFileName"));
			configurationRet.setVisualFeatureDir(prop.getProperty("visualFeatureDir"));
			configurationRet.setCategoryIdMappingFileName(prop.getProperty("categoryIdMappingFileName"));
			configurationRet.setAlignmentDatasetPartPath(prop.getProperty("alignmentDatasetPartPath"));
			configurationRet.setDictionariesDir(prop.getProperty("dictionariesDir"));
			configurationRet.setImageIdListFirstPartFileName(prop.getProperty("imageIdListFirstPartFileName"));
			configurationRet.setUsedCategoryFileName(prop.getProperty("usedCategoryFileName"));
			configurationRet.setInstanceImagePartPath(prop.getProperty("instanceImagePartPath"));
	 
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}		
		
		return configurationRet;
	}
}
