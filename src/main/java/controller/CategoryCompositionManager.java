package controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.LineNumberReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Random;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.Vector;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import main.Main;

public class CategoryCompositionManager {

	/// Variabile che contiene il mapping degli id delle categorie, la chiave è il mapping, il valore è l'id della categoria.
	private TreeMap<Integer, Integer> categoryIdMapping;

	public CategoryCompositionManager() {
		/// Leggo il file che contiene il mapping delle categorie
		createCategoryIdMapping();

	}

	/**
	 * Legge un file di testo contenente il mapping degli id delle categorie e lo sava in una TreeMap
	 */
	private void createCategoryIdMapping() {
		categoryIdMapping = new TreeMap<Integer, Integer>();
		String line;
		String lineToken[];
		try {
			BufferedReader buffReader = new BufferedReader(new FileReader(Main.categoryIdMappingPath));
			while ((line = buffReader.readLine()) != null) {
				lineToken = line.split("\t");
				categoryIdMapping.put( new Integer(lineToken[0]), new Integer(lineToken[1]) );
			}
			buffReader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Prende i file presenti nella directory passata
	 * @param dirPath String che indica il path assoluto della directory di cui si vogliono prendere i file
	 * @return File[] array contenente i file
	 */
	public static File[] getFiles(String dirPath) {

		/* Get the directory */
		File dir = new File(dirPath);

		/* This filter only returns files */
		FileFilter fileFilter = new FileFilter() {
			public boolean accept(File file) {
				return file.isFile();
			}
		};

		/* Returns the list of file of dir */
		return dir.listFiles(fileFilter);

	}


	/**
	 * Per ogni file, ognuno di una categoria, presente nella directory passata crea un altro file aggiungendoci
	 * degli esempi negativi presi dagli altri file.
	 * @param path
	 */
	public String addNegativeExample(String dirPath, String phase) {
		BufferedReader buffReader;
		BufferedWriter output;
		String line;

		/// Prendo la lista dei file presenti nella directory
		File[] listOfFiles = getFiles(dirPath);

		String dirPathSplit[] = dirPath.split("/");
		/// In trainModelName si trova la stringa che definisce la dimensione dell'immagine
		String trainModelName = dirPathSplit[dirPathSplit.length-1]; 
		String trainModelPath = new String();
		for(int i=0; i<dirPathSplit.length-1; i++) {
			trainModelPath += dirPathSplit[i] +"/";
		}

		String dstFolder;
		if(phase.equals("train")) {
			dstFolder = "forTrainBalanced";
		} else {
			dstFolder = "toPredictBalanced";
		}

		trainModelPath +=  dstFolder +"/" +trainModelName +"/";

		/// Scrivo su un file tutti i path dove si trovano i file creati per l'addestramento delle SVM
		FileManager.writeFilePath(trainModelPath, phase);

		/// Per ogni file aggiungo il doppio delle sue linee prendendole da gli altri file cambiando il +1 con -1
		for(int idx = 0; idx<listOfFiles.length; idx++) {
			File src = listOfFiles[idx];
			System.out.println("File sorgente: " +src.getName());
			/// Conto le linee presenti nel file
			LineNumberReader lnr;
			long lineNumber = 0;
			try {
				lnr = new LineNumberReader(new FileReader(src));
				lnr.skip(Long.MAX_VALUE);
				lineNumber = lnr.getLineNumber() - 1; // tolgo uno perchè non devo considerare la prima linea e perchè c'è una linea vuota infondo
				System.out.println("lineNumber: " +lineNumber);
				lnr.close();
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	

			System.out.println(listOfFiles.length);
			long lineNumberForCategory = (lineNumber * 2) / (listOfFiles.length-1);
			long restLineNumberForCategory = lineNumber*2 - (lineNumberForCategory*(listOfFiles.length-1));
			System.out.println("lineNumberForCategory: " +lineNumberForCategory +" restLineNumberForCategory: " +restLineNumberForCategory);

			/// Creo una copia del file sorgente dove andare a mettere le altre linee
			String fileName = src.getName();
			String[] dstFileName = fileName.split("\\.");
			System.out.println("Scrittura file in: " +trainModelPath);
			File dst = new File(trainModelPath +"/" +dstFileName[0] +".dat");
			try {
				//Files.copy(src.toPath(), dst.toPath());
				copyFile(src, dst);

				/// Apro il file dst dove devo fare l'append degli esempi negativi
				output = new BufferedWriter(new FileWriter(dst, true));

				/// Ciclo sui file ed aggiungo gli esempi negativi
				for(int idx2 = 0; idx2<listOfFiles.length; idx2++) {
					File toAppend = listOfFiles[idx2];
					/// Se è il file della categoria che ho apero passo al successivo
					if(toAppend.equals(src)) {
						continue;
					}
					Vector<String> lineVector = new Vector<String>();

					/// Leggo tutte le linee del file e le inserisco in un vettore di stringhe
					buffReader = new BufferedReader(new FileReader(toAppend));
					line = buffReader.readLine(); // Leggo la prima linea senza salvarla perchè contiene un commento
					while ((line = buffReader.readLine()) != null) {
						line = line.replace("+1", "-1");
						lineVector.add(line);
					}
					buffReader.close();

					/// Aggiungo al file destinazione tante righe quanto vale il contatore lineNumberForCategory
					for(int i = 0; i<lineNumberForCategory; i++) {
						Random rand = new Random();

						int  n = rand.nextInt(lineVector.size()-1);
						output.write(lineVector.remove(n) +"\n");
					}

					if(restLineNumberForCategory > 0) {
						Random rand = new Random();
						int  n = rand.nextInt(lineVector.size()-1);
						output.write(lineVector.remove(n) +"\n");
						restLineNumberForCategory--;
					}

				}

				output.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if(restLineNumberForCategory>0) {
				System.out.println("Attenzione restLineNumberForCategory è maggiore di 0! (" +restLineNumberForCategory +")");
			}

		}
		
		return trainModelPath;

	}

	/**
	 * Copia il file sorgente in un file destinazione saltando la prima linea
	 * @param src File sorgente
	 * @param dst File destinazione
	 */
	private void copyFile(File src, File dst) {
		Scanner fileScanner;
		try {
			fileScanner = new Scanner(src);

			FileWriter fileStream = new FileWriter(dst);
			BufferedWriter out = new BufferedWriter(fileStream);
			String next = fileScanner.nextLine(); // Salto la prima linea perchè è un commento
			while(fileScanner.hasNextLine()) {
				next = fileScanner.nextLine();
				if(next.equals("\n")) 
					out.newLine();
				else 
					out.write(next);
				out.newLine();   
			}
			out.close();	
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

	/**
	 * Per ogni file, ognuno di una categoria, presente nella directory passata crea un altro file aggiungendoci
	 * tutti esempi degli altri file come negativi.
	 * @param path
	 */
	public void addNegativeExampleNotBalanced(String dirPath, String phase) {

		BufferedReader buffReader;
		BufferedWriter output;
		String line;

		/// Prendo la lista dei file presenti nella directory
		File[] listOfFiles = getFiles(dirPath);

		/// Per ogni file aggiungo le linee di tutti gli altri cambiando il +1 con -1
		for(int idx = 0; idx<listOfFiles.length; idx++) {
			File src = listOfFiles[idx];
			/// Creo una copia del file sorgente dove andare a mettere le altre linee
			String fileName = src.getName();
			String[] dstFileName = fileName.split("\\.");
			File dst = new File(dirPath +"/" +dstFileName[0] +"_wNEG_nB.dat");
			try {
				Files.copy(src.toPath(), dst.toPath());

				/// Apro il file dst dove devo fare l'append degli esempi negativi
				output = new BufferedWriter(new FileWriter(dst, true));

				/// Ciclo sui file ed aggiungo gli esempi negativi
				for(int idx2 = 0; idx2<listOfFiles.length; idx2++) {
					File toAppend = listOfFiles[idx2];
					if(toAppend.equals(src)) {
						continue;
					}

					buffReader = new BufferedReader(new FileReader(toAppend));
					while ((line = buffReader.readLine()) != null) {
						line = line.replace("+1", "-1");
						output.write(line);
					}
					buffReader.close();
				}

				output.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	/**
	 * Crea un unico file contenente gli esempi di tutte le categorie, le categorie vengono rinominate in maniera
	 * lineare, e quindi viene anche creato un file con il mapping
	 * @param phase 
	 * @param path
	 */
	public String writeOneFile(String dirPath, String phase) {

		BufferedReader buffReader;
		BufferedWriter output;
		String line;
		String writeFilePath = null;

		int count = 0;
		try {
			String dirPathSplit[] = dirPath.split("/");
			/// In trainModelName si trova la stringa che definisce la dimensione dell'immagine
			String trainModelName = dirPathSplit[dirPathSplit.length-1]; 
			/// Costruisco il path dove andranno scritti i file
			String trainModelPath = new String();
			for(int i=0; i<dirPathSplit.length-1; i++) {
				trainModelPath += dirPathSplit[i] +"/";
			}

			String dstFolder;
			if(phase.equals("train") || phase.equals("trainval")) {
				dstFolder = "forTrain";
			} else {
				dstFolder = "toPredict";
			}

			trainModelPath += dstFolder +"/";
			
			/// Creo le directory se non ci sono già
			new File(trainModelPath).mkdirs();

			/// Scrivo su un file tutti i path dei file di train creati
			writeFilePath = trainModelPath +trainModelName +"_"+ dstFolder +".dat";
			System.out.println("Scrittura file: " +writeFilePath);
			FileManager.writeFilePath(writeFilePath, phase);

			/// Apro il file dove inserire tutte le linee
			File dst = new File(writeFilePath);
			output = new BufferedWriter(new FileWriter(dst, true));

			/// Per ogni categoria leggo il file corrispondente ed inserisco le line nel file di destinazione
			for(Integer key : categoryIdMapping.keySet()) {
				Integer categoryId = categoryIdMapping.get(key);

				buffReader = new BufferedReader(new FileReader(dirPath +"/" +categoryId +".dat"));
				line = buffReader.readLine(); // Salto la prima linea che contiene il commento, per liblinear non serve
				//System.out.println("Linea saltata: " +line);
				int linecount = 0;
				while ((line = buffReader.readLine()) != null) {
					linecount++;
					line = line.replace("+1", key.toString());
					output.write(line +"\n");
				}
				buffReader.close();

				//System.out.println("cat: " +categoryId[0] +" linee " +linecount);

				count++;
			}

			//System.out.println("Numero categorie: " +count);

			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return writeFilePath;
	}
	
	/**
	 * Metodo che si occupa di unire i file di train e val delle diverse categorie formando i file di trainval per le diverse categorie
	 * @param dirPath String contenente il path della cartella contenente i file del train, path da cui vengono costruiti quelli per il val e per il trainval
	 */
	public void writeTrainvalFile(String dirPath) {
		String phase = "trainval";
		/// Costruisco il path della cartella che contiene i file di train
		String trainDirPath = dirPath;
		/// Costruisco il path della cartella che contiene i file di val
		String valDirPath = dirPath.replace("train", "val");
		/// Costruisco il path della cartella che conterà i file di trainval
		String trainvalDirPath = dirPath.replace("train", "trainval");
		/// Scrivo su un file i path che contengono i file di trainval per le diverse categorie
		FileManager.writeFilePath(trainvalDirPath, phase);

		/// Se la cartella di destinazione dei file di trainval non esiste la creo
		File destination = new File(trainvalDirPath);
		if(!destination.exists()) {
			destination.mkdirs();
		}
		
		/// Per ogni categoria
		for(Integer categoryID : categoryIdMapping.values()) {
			/// faccio il merge del file di train e del file di val di quella categoria
			String trainFileAbsolutePath = trainDirPath +"/" +categoryID +".dat";
			String valFileAbsolutePath = valDirPath +"/" +categoryID +".dat";
			String trainvalFileAbsolutePath = trainvalDirPath +"/" +categoryID +".dat";
			
			/// Preparo il vettore che i descrittori dei file da unire
			File writeFileToMerge[] = new File[2];
			writeFileToMerge[0] = new File(trainFileAbsolutePath);
			writeFileToMerge[1] = new File(valFileAbsolutePath);
			// Preparo il descrittore del file che conterrà l'unione
			destination = new File(trainvalFileAbsolutePath);
						
			// Eseguo il merge dei file
			try {
				//mergeFiles(destination, writeFileToMerge);
				FileUtils.copyFile(writeFileToMerge[0], destination);
				
				String line;
				BufferedReader buffReader = new BufferedReader(new FileReader(writeFileToMerge[1]));
				BufferedWriter buffWriter = new BufferedWriter(new FileWriter(destination, true));
				buffReader.readLine(); // Salto la prima linea, ci sono i commenti
				while ((line = buffReader.readLine()) != null) {
				      buffWriter.write(line +"\n");
				}
				
				buffReader.close();
				buffWriter.close();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
	/**
	 * Crea un unico file contenente 200 instance prese random per ogni categoria dai file del testset.
	 */
	public String writeTestSetWithXinstanceForCategory(String dirPath, String phase, int numberOfInstanceForCategory) {
		
		Random random = new Random();

		BufferedReader buffReader;
		BufferedWriter output;
		String line;
		String writeFilePath = null;
		/// Vettore che contiene tutte le instance della categoria corrente
		Vector<String> featureLinesOfCategory;

		try {
			String dirPathSplit[] = dirPath.split("/");
			/// In trainModelName si trova la stringa che definisce la dimensione dell'immagine
			String trainModelName = dirPathSplit[dirPathSplit.length-1]; 
			/// Costruisco il path dove andranno scritti i file
			String trainModelPath = new String();
			for(int i=0; i<dirPathSplit.length-1; i++) {
				trainModelPath += dirPathSplit[i] +"/";
			}

			/// TODO [writeTestSetWithXinstanceForCategory] dovrei poter eliminare questa parte, visto che tanto devo usare questo metodo solo per il test
			String dstFolder;
			if(phase.equals("train") || phase.equals("trainval")) {
				dstFolder = "forTrain";
			} else {
				dstFolder = "toPredict";
			}
			trainModelPath += dstFolder +"/";
			
			/// Creo le directory se non ci sono già
			new File(trainModelPath).mkdirs();

			/// Scrivo su un file tutti i path dei file di train creati
			writeFilePath = trainModelPath +trainModelName +"_"+ dstFolder +numberOfInstanceForCategory +".dat";
			FileManager.writeFilePath(writeFilePath, phase);

			/// Apro il file dove inserire tutte le linee
			File dst = new File(writeFilePath);
			output = new BufferedWriter(new FileWriter(dst, true));

			/// Per ogni categoria leggo il file corrispondente e salvo le linee nel vettore.
			for(Integer key : categoryIdMapping.keySet()) {
				Integer categoryId = categoryIdMapping.get(key);
				
				featureLinesOfCategory = new Vector<String>(); 

				buffReader = new BufferedReader(new FileReader(dirPath +"/" +categoryId +".dat"));
				line = buffReader.readLine(); // Salto la prima linea che contiene il commento, per liblinear non serve
				//System.out.println("Linea saltata: " +line);
				while ((line = buffReader.readLine()) != null) {
					line = line.replace("+1", key.toString());
					featureLinesOfCategory.add(line);
				}
				buffReader.close();

				/// Prende numberOfInstanceForCategory vettori di feature casuali e li salva nel file
				for(int countNumOfInstanceForCategory=0; countNumOfInstanceForCategory<numberOfInstanceForCategory; countNumOfInstanceForCategory++) {
					int featureLinesOfCategorySize = featureLinesOfCategory.size();
					int k = random.nextInt(featureLinesOfCategorySize); //Valori compresi tra 0 e featureLinesOfCategorySize
					
					/// Rimuovo un elemento random dal vettore e lo salvo sul file di destinazione
					output.write(featureLinesOfCategory.remove(k) +"\n");
					
				}
				
			}

			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return writeFilePath;
	}
	
	/**
	 * Data una lista file, ne ritorna uno solo che è il merge dei file nella lista.
	 * @param destination File di destinazione contenente il merge dei file sorgenti
	 * @param sources File[] array dei file di cui deve essere eseguito il merge
	 * @throws IOException
	 */
	private void mergeFiles(File destination, File[] sources) 
			throws IOException {
		OutputStream output = null;
		try {
			output = createAppendableStream(destination);
			for (File source : sources) {
				appendFile(output, source);
			}
		} finally {
			IOUtils.closeQuietly(output);
		}		
	}


	private static BufferedOutputStream createAppendableStream(File destination)
			throws FileNotFoundException {
		return new BufferedOutputStream(new FileOutputStream(destination, true));
	}

	private static void appendFile(OutputStream output, File source)
			throws IOException {
		InputStream input = null;
		try {
			input = new BufferedInputStream(new FileInputStream(source));
			IOUtils.copy(input, output);
		} finally {
			IOUtils.closeQuietly(input);
		}
	}


}
