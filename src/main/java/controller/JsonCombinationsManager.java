package controller;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Vector;

import model.Feature;
import model.FeatureHOG;
import model.FeatureSIFT;
import model.FeatureType;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonCombinationsManager {

	private ObjectMapper mapper;

	private JsonNode rootNodeSentences;

	/**
	 * Apre e legge il parse tree dal file json
	 * 
	 */
	public void openJsonFeature() {
		mapper = new ObjectMapper();

		File jsonSentences = new File("/media/coman/Tesi/MicrosoftCOCO/combinations.json");	

		try {
			// Lettura dell'albero del json
			rootNodeSentences = mapper.readTree(jsonSentences);

		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Vector<Vector<Feature>> loadCombinations() {
		Vector<Vector<Feature>> returnVector = new Vector<Vector<Feature>>();

		Vector<Feature> combinationVector;

		JsonNode combinations = rootNodeSentences.path("combinations");

		Iterator<JsonNode> ite = combinations.elements();
		while(ite.hasNext()) {
			combinationVector = new Vector<Feature>();
			
			JsonNode combination = ite.next();
			
			Iterator<JsonNode> featIte = combination.elements();
			while(featIte.hasNext()) {
				JsonNode feature = ite.next();
				
				String featureType = feature.path("type").textValue();
				int width = feature.path("width").intValue();
				int height = feature.path("height").intValue();
				
				System.out.println(width +" " +height);
				
				if(featureType.equals(FeatureType.HOG)) {
					combinationVector.add(new FeatureHOG(width, height));
					continue;
				}
				int dictSize = feature.path("dictSize").intValue();

				if(featureType.equals(FeatureType.SIFT)) {
					combinationVector.add(new FeatureSIFT(width, height, dictSize));
					continue;					
				}
			}
			
			returnVector.add(combinationVector);
		}

		return returnVector;
	}

	/**
	 * Carica tutti i tipi di visual Feature dal file json
	 * @return Ritorna un Vector contenente VisualFeatureData

	public Vector<VisualFeatureData> loadVisualFeature() {
		String id;
		String type;
		int training_width;
		int training_height;
		boolean black_background;

		Vector<VisualFeatureData> visualFeatures = new Vector<VisualFeatureData>();

		// Lettura dell'array con i dati delle sentences
		JsonNode feats = rootNodeSentences.path("feats");

		// Itero sulle feature presenti nel json
		Iterator<JsonNode> ite = feats.elements(); 
		while (ite.hasNext()) {
			JsonNode tmpVisualFeatData = ite.next();
			id = tmpVisualFeatData.path("id").textValue();
			type = tmpVisualFeatData.path("type").textValue();
			training_width = tmpVisualFeatData.path("training_width").intValue();
			training_height = tmpVisualFeatData.path("training_height").intValue();
			black_background = tmpVisualFeatData.path("black_background").booleanValue();

			VisualFeatureData visualFeatData = new VisualFeatureData(id, type, training_height, training_width, black_background);

			visualFeatures.add(visualFeatData);

		}
		return visualFeatures;

	}

	 */
}
