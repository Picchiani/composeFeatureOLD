package controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import main.Main;
import model.Feature;
import model.FeatureBoN;
import model.FeatureBoNA;
import model.FeatureBoNAV;
import model.FeatureLSABoN;
import model.FeatureLSABoNA;
import model.FeatureLSABoNAV;
import model.FeatureLine;
import model.FeatureType;
import model.FeatureValue;
import model.FeaturenLSABoN;
import model.TextFeature;
import model.VisualFeature;

public class CombineFeatureVectorManager {
	
	/// Contiene i path delle combinazioni per poi salvarli su file
	private	Vector<String> combinationPaths = new Vector<String>();

	private boolean addBagOfWordFeature = true;
	private boolean addLSAFeature = true;
	private boolean addNormalizedLSAFeature = false;

	/// Vettore che contiene gli id delle categorie
	private Vector<String> categoryIds = new Vector<String>();
	/// Contengono i vettori delle textFeature per le immagini
	private HashMap<String, Vector<FeatureValue>> bagOfNMap;
	private HashMap<String, Vector<FeatureValue>> bagOfNAMap;
	private HashMap<String, Vector<FeatureValue>> bagOfNAVMap;
	/// Contengono i vettori delle LSAFeature per le immagini (Normalizzati o non)
	private HashMap<String, Vector<FeatureValue>> LSABagOfNMap;
	private HashMap<String, Vector<FeatureValue>> LSABagOfNAMap;
	private HashMap<String, Vector<FeatureValue>> LSABagOfNAVMap;

	
	/**
	 * Funzione di utilità che legge le categorie che si stanno usando da un file, e le inserisce nel vettore categoryIds
	 */
	private void readCategoryIds() {
		FileReader fileReader;
		String line;
		String lineToken[];
		try {
			fileReader = new FileReader(Main.categoryIdMappingPath);
			BufferedReader buffReader = new BufferedReader(fileReader);

			while(true) {
				line = buffReader.readLine();
				if(line == null)
					break;
				
				lineToken = line.split("\t");
				categoryIds.add(lineToken[1]);
			}
			buffReader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public void combineFeature(Vector<Vector<Feature>> combinations) {
		combinationPaths = new Vector<String>();

		String dirSize = new String();
		
		Feature featureBoN = null;
		Feature featureBoNA = null;
		Feature featureBoNAV = null;
		Feature featureLSABoN = null;
		Feature featureLSABoNA = null;
		Feature featureLSABoNAV = null;

		/// Carico le informazioni per i vettori BoWord delle immagini
		if(addBagOfWordFeature) {
			System.out.println("Lettura feature testuali...");
			featureBoN = new FeatureBoN();
			bagOfNMap = loadBagOfWordFeatureRepresentation(featureBoN);	
//			featureBoNA = new FeatureBoNA();
//			bagOfNAMap = loadBagOfWordFeatureRepresentation(featureBoNA);	
//			featureBoNAV = new FeatureBoNAV();
//			bagOfNAVMap = loadBagOfWordFeatureRepresentation(featureBoNAV);
			System.out.println("Fine lettura feature testuali.");
		}
		
		if(addLSAFeature) {
			System.out.println("Lettura feature LSA...");
			featureLSABoN = new FeatureLSABoN();
			LSABagOfNMap = loadBagOfWordFeatureRepresentation(featureLSABoN);
//			featureLSABoNA = new FeatureLSABoNA();
//			LSABagOfNAMap = loadBagOfWordFeatureRepresentation(featureLSABoNA);	
//			featureLSABoNAV = new FeatureLSABoNAV();
//			LSABagOfNAVMap = loadBagOfWordFeatureRepresentation(featureLSABoNAV);
			System.out.println("Fine lettura feature LSA.");
		} else if(addNormalizedLSAFeature) {
			System.out.println("Lettura feature Normalized LSA...");
			featureLSABoN = new FeaturenLSABoN();
			LSABagOfNMap = loadBagOfWordFeatureRepresentation(featureLSABoN);
			System.out.println("Fine lettura feature Normalized LSA.");
		}
		
		readCategoryIds();

		/// Per ogni combinazione
		for(Vector<Feature> combination : combinations) {
			
			System.out.println("Combinazione: ");
			for(Feature feature : combination) {
				System.out.print(feature.getInfo() +" ");
			}
			System.out.println();
			
			/// Per ogni categoria
			for(String categoryId : categoryIds) {
				ArrayList<String> combinedFeature = new ArrayList<String>();
				System.out.println("Scrittura categoria " +categoryId +"...");
				String dirName = new String(); 
				String firstLine = new String();
				
				/// Per ogni feature nella combinazione
				int currentSize = 0;
				int countFeature = 0;
				for(Feature feature : combination) {
					countFeature++;
					dirSize = ((VisualFeature) feature).getWidth() +"_" +((VisualFeature) feature).getHeight();
					/// Costruisco la dir dove mettere i file
					dirName += feature.getDirNamePart();
					/// Costruisco la linea di commento che andrà nel file
					firstLine += feature.getInfo();
					/// Leggo il file contenente i valori delle feature
					String visualFeatureFileName = Main.visualFeatureDir +Main.phase +"/" +feature.featurePath() +categoryId +".dat";
					System.out.println(visualFeatureFileName);
					try {
						
						BufferedReader buffReader = new BufferedReader(new FileReader(visualFeatureFileName));

						/// Leggo la prima linea del file dove sono contenuti i commenti
						String line = buffReader.readLine();
						String[] lineToken = line.split(" ");
						// Prendo il numero di feature
						int sizeFeature = Integer.valueOf(lineToken[2]);
						//System.out.println("Size feature of " +lineToken[1] +": " +sizeFeature);
						int countLine = 0;
						/// Per ogni linea leggo tutte le feature e le aggiungo ad un vettore
						while(true) {
							line = buffReader.readLine();
							if(line == null)
								break;

							//System.out.println("[addBagOfNounFeature] " +line);

							FeatureLine featLine = featureLineSplit(line);
							String featureLine;
							if(currentSize == 0) {
								featureLine = "+1 ";
								Vector<FeatureValue> featureValues = featLine.getFeatureValues();
								for(FeatureValue featValue : featureValues) {
									featureLine += featValue.getPos()+":"+featValue.getValue() +" ";
								}
								
								if(countFeature == combination.size()) {
									String key = featLine.getImageId() +"_" +featLine.getInstanceId();
									featureLine += "# " +key;
								}
								
								combinedFeature.add(featureLine);

							} else {
								featureLine = combinedFeature.get(countLine);
								Vector<FeatureValue> featureValues = featLine.getFeatureValues();
								for(FeatureValue featValue : featureValues) {
									int pos = featValue.getPos()+currentSize;
									featureLine += pos+":"+featValue.getValue() +" ";
								}
							
								if(countFeature == combination.size()) {
									String key = featLine.getImageId() +"_" +featLine.getInstanceId();
									featureLine += "# " +key;
								}
								combinedFeature.set(countLine, featureLine);
							}
							
							countLine++;
							
//							if(countLine % 1000 == 0) {
//								System.out.println(countLine);
//							}
						}
						buffReader.close();
						//System.out.println("CountLine: " +countLine);
						currentSize += sizeFeature;

					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					System.out.println("Finita lettura: " +feature.getInfo() );
				}
				
				/// Scrivo le feature concatenate su file
				writeCombinedFeatureValue(combinedFeature, "# combination " +currentSize +" " +firstLine, dirSize, dirName, categoryId);
			
				/// Aggiungo alla combinazione le informazioni BoWord
				addTextFeature(combinedFeature, currentSize, firstLine, dirSize, dirName, categoryId, featureBoN, featureLSABoN);

//				addTextFeature(ret, vectorSize, firstLine, dirSize, dirName, categoryId, featureBoNA, featureLSABoNA);

//				addTextFeature(combinedFeature, currentSize, firstLine, dirSize, dirName, categoryId, featureBoNAV, featureLSABoNAV);
			}
		}
	}

	/**
	 * 
	 */
	public void composeVisualFeatureVector(Vector<Vector<Feature>> combinations) {
		combinationPaths = new Vector<String>();
		HashMap<String, Vector<FeatureValue>> ret = new HashMap<String, Vector<FeatureValue>>();
		
		int vectorSize = 0;
		String dirName = null, firstLine = null;
		String dirSize = null;

		Feature featureBoN = null;
		Feature featureBoNA = null;
		Feature featureBoNAV = null;
		Feature featureLSABoN = null;
		Feature featureLSABoNA = null;
		Feature featureLSABoNAV = null;

		/// Carico le informazioni per i vettori BoWord delle immagini
		if(addBagOfWordFeature) {
			System.out.println("Lettura feature testuali...");
			featureBoN = new FeatureBoN();
			bagOfNMap = loadBagOfWordFeatureRepresentation(featureBoN);	
			featureBoNA = new FeatureBoNA();
			bagOfNAMap = loadBagOfWordFeatureRepresentation(featureBoNA);	
			featureBoNAV = new FeatureBoNAV();
			bagOfNAVMap = loadBagOfWordFeatureRepresentation(featureBoNAV);
			System.out.println("Fine lettura feature testuali.");
		}
		
		if(addLSAFeature) {
			System.out.println("Lettura feature LSA...");
			featureLSABoN = new FeatureLSABoN();
			LSABagOfNMap = loadBagOfWordFeatureRepresentation(featureLSABoN);	
			featureLSABoNA = new FeatureLSABoNA();
			LSABagOfNAMap = loadBagOfWordFeatureRepresentation(featureLSABoNA);	
			featureLSABoNAV = new FeatureLSABoNAV();
			LSABagOfNAVMap = loadBagOfWordFeatureRepresentation(featureLSABoNAV);
			System.out.println("Fine lettura feature LSA.");
		}

		readCategoryIds();

		/// Per ogni combinazione
		for(Vector<Feature> combination : combinations) {
			
			System.out.println("Combinazione: ");
			for(Feature feature : combination) {
				System.out.print(feature.getInfo() +" ");
			}
			System.out.println();
			
			/// Per ogni categoria
			for(String categoryId : categoryIds) {
				System.out.println("Scrittura categoria " +categoryId +"...");
				dirName = new String(); 
				firstLine = new String();

				/// Per ogni feature nella combinazione
				for(Feature feature : combination) {
					dirSize = ((VisualFeature) feature).getWidth() +"_" +((VisualFeature) feature).getHeight();
					/// Costruisco la dir dove mettere i file
					dirName += feature.getDirNamePart();
					/// Costruisco la linea di commento che andrà nel file
					firstLine += feature.getInfo();
					/// Leggo il file contenente i valori delle feature
					String visualFeatureFileName = Main.visualFeatureDir +Main.phase +"/" +feature.featurePath() +categoryId +".dat";
					System.out.println(visualFeatureFileName);
					try {
						BufferedReader buffReader = new BufferedReader(new FileReader(visualFeatureFileName));

						/// Leggo la prima linea del file dove sono contenuti i commenti
						String line = buffReader.readLine();
						String[] lineToken = line.split(" ");
						// Prendo il numero di feature
						int sizeFeature = Integer.valueOf(lineToken[2]);
						//System.out.println("Size feature of " +lineToken[1] +": " +sizeFeature);
						int countLine = 0;
						/// Per ogni linea leggo tutte le feature e le aggiungo ad un vettore
						while(true) {
							line = buffReader.readLine();
							if(line == null)
								break;
							countLine++;

							FeatureLine featLine = featureLineSplit(line);
							String key = featLine.getImageId() +"_" +featLine.getInstanceId();
							if(ret.containsKey(key)) {
								Vector<FeatureValue> vec = ret.get(key);
								for(FeatureValue featValue : featLine.getFeatureValues()) {
									featValue.incrementPos(vectorSize);
									vec.add(featValue);
								}
								ret.put(key, vec);
							} else {
								ret.put(key, featLine.getFeatureValues());
							}
							
							if(countLine % 4000 == 0) {
								System.out.println(countLine);
							}
						}
						buffReader.close();
						//System.out.println("CountLine: " +countLine);
						vectorSize += sizeFeature;

					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					System.out.println("Finita lettura: " +feature.getInfo() );

				}

				/// Scrivo le feature concatenate su file
				writeCombinationValue(ret, "# combination " +vectorSize +" " +firstLine, dirSize, dirName, categoryId);

				/// Aggiungo alla combinazione le informazioni BoWord
				addTextFeature(ret, vectorSize, firstLine, dirSize, dirName, categoryId, featureBoN, featureLSABoN);

//				addTextFeature(ret, vectorSize, firstLine, dirSize, dirName, categoryId, featureBoNA, featureLSABoNA);

				addTextFeature(ret, vectorSize, firstLine, dirSize, dirName, categoryId, featureBoNAV, featureLSABoNAV);

//				if(addBagOfWordFeature) {
//					int newVectorSize = vectorSize;
//					//System.out.println("Original Vector size: " +vectorSize);
//					String newFirstLine = firstLine;
//					String newDirName = dirName;
//					
//					HashMap<String, Vector<FeatureValue>> newRet = addBagOfWordFeature(ret, newVectorSize, (TextFeature) featureBoN);
//					
//					newVectorSize += ((TextFeature) featureBoN).getDictionarySize();
//					//System.out.println("Original Vector size+BoN: " +newVectorSize);
//
//					newFirstLine += featureBoN.getInfo();
//					newDirName += featureBoN.getDirNamePart(); 
//					writeCombinationValue(newRet, "# combination " +newVectorSize +" " +newFirstLine, dirSize, newDirName, categoryId);
//
//					if(addLSAFeature) {
//						HashMap<String, Vector<FeatureValue>> finalRet = addLSABagOfWordFeature(newRet, newVectorSize, (TextFeature) featureLSABoN);
//						newVectorSize += ((TextFeature) featureLSABoN).getDictionarySize();
//						//System.out.println("Original Vector size+BoN+LSABoN: " +newVectorSize);
//						writeCombinationValue(finalRet, "# combination " +newVectorSize +" " +newFirstLine +" " +featureLSABoN.getInfo(), dirSize, newDirName+featureLSABoN.getDirNamePart(), categoryId);
//					}
//
//					newVectorSize = vectorSize;
//					//System.out.println("Original Vector size: " +vectorSize);
//					newFirstLine = firstLine;
//					newDirName = dirName;
//					newRet = addBagOfWordFeature(ret, newVectorSize, (TextFeature) featureBoNA);
//					newVectorSize += ((TextFeature) featureBoNA).getDictionarySize();
//
//					//System.out.println("Original Vector size+BoNA: " +newVectorSize);
//					newFirstLine += featureBoNA.getInfo();
//					newDirName += featureBoNA.getDirNamePart(); 
//					writeCombinationValue(newRet, "# combination " +vectorSize +" " +newFirstLine, dirSize, newDirName, categoryId);
//
//					if(addLSAFeature) {
//						HashMap<String, Vector<FeatureValue>> finalRet = addLSABagOfWordFeature(newRet, newVectorSize, (TextFeature) featureLSABoNA);
//						newVectorSize += ((TextFeature) featureLSABoNA).getDictionarySize();
//						//System.out.println("Original Vector size+BoN+LSABoNA: " +newVectorSize);
//						writeCombinationValue(finalRet, "# combination " +newVectorSize +" " +newFirstLine +" " +featureLSABoNA.getInfo(), dirSize, newDirName+featureLSABoNA.getDirNamePart(), categoryId);
//					}
//
//					newVectorSize = vectorSize;
//					newFirstLine = firstLine;
//					newDirName = dirName;
//					//System.out.println("Original Vector size+BoNAV: " +newVectorSize);
//					newRet = addBagOfWordFeature(ret, newVectorSize, (TextFeature) featureBoNAV);
//					newVectorSize += ((TextFeature) featureBoNAV).getDictionarySize();
//					newFirstLine += featureBoNAV.getInfo();
//					newDirName += featureBoNAV.getDirNamePart(); 
//					writeCombinationValue(newRet, "# combination " +newVectorSize +" " +newFirstLine, dirSize, newDirName, categoryId);
//
//					if(addLSAFeature) {
//						HashMap<String, Vector<FeatureValue>> finalRet = addLSABagOfWordFeature(newRet, newVectorSize, (TextFeature) featureLSABoNAV);
//						newVectorSize += ((TextFeature) featureLSABoNAV).getDictionarySize();
//						//System.out.println("Original Vector size+BoN+LSABoNA: " +newVectorSize);
//						writeCombinationValue(finalRet, "# combination " +newVectorSize +" " +newFirstLine +" "  +featureLSABoNAV.getInfo(), dirSize, newDirName+featureLSABoNAV.getDirNamePart(), categoryId);
//					}
//
//				}
				
				vectorSize = 0;
				ret = new HashMap<String, Vector<FeatureValue>>();
			}

		}

		//		System.out.println("Ret size: " +ret.size());
		//		for(Map.Entry<String, Vector<FeatureValue>> featureValue : ret.entrySet()) {
		//			System.out.println("Size" +featureValue.getValue().size());
		//			System.out.println(featureValue.getValue().lastElement().getPos());
		//		}

	}
	
	private void addTextFeature(HashMap<String, Vector<FeatureValue>> combinedFeature, int vectorSize, String firstLine,
			String dirSize, String dirName, String categoryId, Feature featureToAdd, Feature featureLSAToAdd) {
		int newVectorSize = vectorSize;
		//System.out.println("Original Vector size: " +vectorSize);
		String newFirstLine = firstLine;
		String newDirName = dirName;
		
		HashMap<String, Vector<FeatureValue>> newCombination = addBagOfWordFeature(combinedFeature, newVectorSize, (TextFeature) featureToAdd);
		
		newVectorSize += ((TextFeature) featureToAdd).getDictionarySize();
		//System.out.println("Original Vector size+BoN: " +newVectorSize);

		newFirstLine += featureToAdd.getInfo();
		newDirName += featureToAdd.getDirNamePart(); 
		writeCombinationValue(newCombination, "# combination " +newVectorSize +" " +newFirstLine, dirSize, newDirName, categoryId);

		if(addLSAFeature) {
			HashMap<String, Vector<FeatureValue>> finalCombination = addLSABagOfWordFeature(newCombination, newVectorSize, (TextFeature) featureLSAToAdd);
			newVectorSize += ((TextFeature) featureLSAToAdd).getDictionarySize();
			//System.out.println("Original Vector size+BoN+LSABoN: " +newVectorSize);
			writeCombinationValue(finalCombination, "# combination " +newVectorSize +" " +newFirstLine +" " +featureLSAToAdd.getInfo(), dirSize, newDirName+featureLSAToAdd.getDirNamePart(), categoryId);
		}

	}
	
	private void addTextFeature(ArrayList<String> combinedFeature, int vectorSize, String firstLine,
			String dirSize, String dirName, String categoryId, Feature featureToAdd, Feature featureLSAToAdd) {
		int newVectorSize = vectorSize;
		//System.out.println("Original Vector size: " +vectorSize);
		String newFirstLine = firstLine;
		String newDirName = dirName;
		
		ArrayList<String> newCombination = addBagOfWordFeature(combinedFeature, newVectorSize, (TextFeature) featureToAdd);
		
		newVectorSize += ((TextFeature) featureToAdd).getDictionarySize();
		//System.out.println("Original Vector size+BoN: " +newVectorSize);

		newFirstLine += featureToAdd.getInfo();
		newDirName += featureToAdd.getDirNamePart(); 
		writeCombinedFeatureValue(newCombination, "# combination " +newVectorSize +" " +newFirstLine, dirSize, newDirName, categoryId);

		if(addLSAFeature || addNormalizedLSAFeature) {
			ArrayList<String> finalCombination = addLSABagOfWordFeature(newCombination, newVectorSize, (TextFeature) featureLSAToAdd);
			newVectorSize += ((TextFeature) featureLSAToAdd).getDictionarySize();
			//System.out.println("Original Vector size+BoN+LSABoN: " +newVectorSize);
			writeCombinedFeatureValue(finalCombination, "# combination " +newVectorSize +" " +newFirstLine +" " +featureLSAToAdd.getInfo(), dirSize, newDirName+featureLSAToAdd.getDirNamePart(), categoryId);
		}
		
	}

	/**
	 * Metodo che si occupa di parsare una linea dividendo le feature dal commento in coda alla linea
	 * @param line Stringa che rappresenta la linea da parsare
	 * @return Ritorna un FeatureLine
	 */
	public FeatureLine featureLineSplit(String line) {
		Vector<FeatureValue> featureValues = new Vector<FeatureValue>();

		String[] lineToken = line.split("#");
		/// Nel primo token si trovano i valori delle feature
		String newLine = lineToken[0];
		/// Nel secondo si trovano le info per quella linea (imageId e instanceId)
		String info = lineToken[1];
		String[] infoToken = info.split(" ");

		String[] featureString = newLine.split(" ");

		/// Parto dal secondo elemento, il primo è il +1
		for(int idx = 1; idx < featureString.length; idx++) {
			String[] featureToken = featureString[idx].split(":");

			//System.out.println("Pos: " +featureToken[0] +" Value: " +featureToken[1]);
			featureValues.add(new FeatureValue(new Integer(featureToken[0]), new BigDecimal(featureToken[1])));
		}

		return new FeatureLine(infoToken[1], infoToken[2], featureValues);

	}

	/**
	 * Scrive su file i valori delle feature contenuti nella HashMap passata.
	 * @param combinationFeatureValue HashMap che contiene i diversi valori delle feature per una data categoria e una combinazione
	 * @param firstLine La prima linea di commento da inserire nel file
	 * @param dirSize Stringa contenente la grandezza delle immagini a cui si riferisce la combinazione
	 * @param dirName Stringa contenente il nome della combinazione e quindi indica i tipi di feature combinati
	 * @param categoryId Stringa contenente l'id della categoria di cui si stanno scrivendo i valori
	 */
	private void writeCombinationValue(HashMap<String, Vector<FeatureValue>> combinationFeatureValue, String firstLine, String dirSize, String dirName, String categoryId) {
		FileWriter fileWriter;

		try {
			String writeDir = Main.datasetAbsolutePath +"combinations/" +Main.phase +"/" +dirName +"/" +dirSize +"/";
			/// Salvo il path per poi scriverlo su file
			if(!combinationPaths.contains(writeDir)) {
				FileManager.writeCombinationPathToFile(writeDir, dirName);
				combinationPaths.add(writeDir);
			}
			
			boolean directoryToWrite = new File(writeDir).mkdirs();
			fileWriter = new FileWriter(writeDir +categoryId +".dat");
			BufferedWriter buffWriter = new BufferedWriter(fileWriter, 262144);

			/// Riga di commento con le feature concatenate nel file
			buffWriter.write(firstLine +"\n");

			/// Per ogni linea
			for(Map.Entry<String, Vector<FeatureValue>> lineVector : combinationFeatureValue.entrySet()) {

				String line = "+1 ";
				Vector<FeatureValue> featureValues = lineVector.getValue();
				
				/// Per ogni valore delle feature
				for(FeatureValue value : featureValues) {
					line += value.getPos() +":" +value.getValue().toString() +" ";
				}
				
				String[] info = lineVector.getKey().split("_");
				line += "# " +info[0] +" " +info[1] +"\n";
				
				buffWriter.write(line);
				buffWriter.flush();
				
			}

			buffWriter.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void writeCombinedFeatureValue(ArrayList<String> combinationFeatureValue, String firstLine, String dirSize, String dirName, String categoryId) {
		FileWriter fileWriter;

		try {
			String writeDir = Main.datasetAbsolutePath +"combinations/" +Main.phase +"/" +dirName +"/" +dirSize +"/";
			System.out.println(writeDir);
			/// Salvo il path per poi scriverlo su file
			if(!combinationPaths.contains(writeDir)) {
				FileManager.writeCombinationPathToFile(writeDir, dirName);
				combinationPaths.add(writeDir);
			}
			
			boolean directoryToWrite = new File(writeDir).mkdirs();
			fileWriter = new FileWriter(writeDir +categoryId +".dat");
			BufferedWriter buffWriter = new BufferedWriter(fileWriter, 262144);

			/// Riga di commento con le feature concatenate nel file
			buffWriter.write(firstLine +"\n");

			/// Per ogni linea
			for(String lineVector : combinationFeatureValue) {
				buffWriter.write(lineVector+"\n");
			}
			
			buffWriter.flush();
			buffWriter.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Alla combinazione passata aggiunge le informazioni BagOfN e poi la ritorna
	 * @param combinationFeatureValue
	 * @param dimension
	 * @return
	 */
	public HashMap<String, Vector<FeatureValue>> addBagOfWordFeature(HashMap<String, Vector<FeatureValue>> combinationFeatureValue, int dimension, TextFeature textFeature) {
		HashMap<String, Vector<FeatureValue>> ret = cloneHashMap(combinationFeatureValue);
		Vector<FeatureValue> bagOfWordMapLine = null;

		for(Map.Entry<String, Vector<FeatureValue>> line : ret.entrySet()) {
			String key = line.getKey();
			//System.out.println("[addBagOfWordFeature] key: " +key);
			Vector<FeatureValue> combinationLine = line.getValue();
			//System.out.println("[addBagOfWordFeature] Prima: " +combinationLine.size());

			String[] lineInfo = key.split("_");
			//System.out.println("[addBagOfWordFeature] imageId key: " +lineInfo[0]);
			if(textFeature.getFeatureType().equals(FeatureType.BoN)) {
				bagOfWordMapLine = bagOfNMap.get(lineInfo[0]);
			} else if(textFeature.getFeatureType().equals(FeatureType.BoNA)) {
				bagOfWordMapLine = bagOfNAMap.get(lineInfo[0]);
			} else {
				bagOfWordMapLine = bagOfNAVMap.get(lineInfo[0]);
			}

			for(FeatureValue featureValue : bagOfWordMapLine) {
				FeatureValue newFeatureValue = new FeatureValue(featureValue.getPos()+dimension, featureValue.getValue());
				//System.out.println(" " +newFeatureValue.getPos() +" " +newFeatureValue.getValue());
				combinationLine.add(newFeatureValue);
			}
			//System.out.println("[addBagOfWordFeature] key: " +key +" dopo: " +combinationLine.size());
			ret.put(key, combinationLine);
		}

		return ret;

	}
	
	public ArrayList<String> addBagOfWordFeature(ArrayList<String> combinationFeatureValue, int dimension, TextFeature textFeature) {
		ArrayList<String> ret = (ArrayList<String>) combinationFeatureValue.clone();
		Vector<FeatureValue> bagOfWordMapLine = null;

		for(String line : ret) {
			String lineToken[] = line.split("# ");
			String newLine = lineToken[0];
			String key = lineToken[1];
			//System.out.println("[addBagOfWordFeature] key: " +key);
			
			String[] lineInfo = key.split("_");
			//System.out.println("[addBagOfWordFeature] imageId key: " +lineInfo[0]);
			if(textFeature.getFeatureType().equals(FeatureType.BoN)) {
				bagOfWordMapLine = bagOfNMap.get(lineInfo[0]);
			} else if(textFeature.getFeatureType().equals(FeatureType.BoNA)) {
				bagOfWordMapLine = bagOfNAMap.get(lineInfo[0]);
			} else {
				bagOfWordMapLine = bagOfNAVMap.get(lineInfo[0]);
			}

			for(FeatureValue featureValue : bagOfWordMapLine) {
				newLine += featureValue.getPos()+dimension+":"+featureValue.getValue() +" ";
				//System.out.println(" " +newFeatureValue.getPos() +" " +newFeatureValue.getValue());
			}
			//System.out.println("[addBagOfWordFeature] key: " +key +" dopo: " +combinationLine.size());
			newLine += "# " +key;
			ret.set(ret.indexOf(line), newLine);
		}

		return ret;

	}
	
	/**
	 * Metodo privato di utilità si occupa di clonare la hashmap che contiene le feature.
	 * @param combinationFeatureValue Hashmap da clonare
	 * @return HashMap clonata.
	 */
	private HashMap<String, Vector<FeatureValue>> cloneHashMap(HashMap<String, Vector<FeatureValue>> combinationFeatureValue) {
		HashMap<String, Vector<FeatureValue>> cloned = new HashMap<String, Vector<FeatureValue>>();
		
		for(Map.Entry<String, Vector<FeatureValue>> line : combinationFeatureValue.entrySet()) {
			Vector<FeatureValue> clonedVector = (Vector) line.getValue().clone();
			cloned.put(line.getKey(), clonedVector);
		}
		
		return cloned;
	}
	
	/**
	 * Alla combinazione passata aggiunge le informazioni LSABagOfWord e poi la ritorna
	 * @param combinationFeatureValue
	 * @param dimension
	 * @return
	 */
	public HashMap<String, Vector<FeatureValue>> addLSABagOfWordFeature(HashMap<String, Vector<FeatureValue>> combinationFeatureValue, int dimension, TextFeature textFeature) {
		HashMap<String, Vector<FeatureValue>> ret = new HashMap<String, Vector<FeatureValue>>(combinationFeatureValue);
		Vector<FeatureValue> bagOfWordMapLine = null;

		for(Map.Entry<String, Vector<FeatureValue>> line : ret.entrySet()) {
			String key = line.getKey();
			//System.out.println("[addBagOfWordFeature] key: " +key);
			Vector<FeatureValue> combinationLine = line.getValue();
			//System.out.println("[addBagOfWordFeature] Prima: " +combinationLine.size());

			String[] lineInfo = key.split("_");
			//System.out.println("[addBagOfWordFeature] imageId key: " +lineInfo[0]);
			if(textFeature.getFeatureType().equals(FeatureType.LSABoN)) {
				bagOfWordMapLine = LSABagOfNMap.get(lineInfo[0]);
			} else if(textFeature.getFeatureType().equals(FeatureType.LSABoNA)) {
				bagOfWordMapLine = LSABagOfNAMap.get(lineInfo[0]);
			} else {
				bagOfWordMapLine = LSABagOfNAVMap.get(lineInfo[0]);
			}

			for(FeatureValue featureValue : bagOfWordMapLine) {
				FeatureValue newFeatureValue = new FeatureValue(featureValue.getPos()+dimension, featureValue.getValue());
				//System.out.println(" " +newFeatureValue.getPos() +" " +newFeatureValue.getValue());
				combinationLine.add(newFeatureValue);
			}
			//System.out.println("[addBagOfWordFeature] key: " +key +" dopo: " +combinationLine.size());
			ret.put(key, combinationLine);
		}

		return ret;

	}
	
	public ArrayList<String> addLSABagOfWordFeature(ArrayList<String> combinationFeatureValue, int dimension, TextFeature textFeature) {
		ArrayList<String> ret = (ArrayList<String>) combinationFeatureValue.clone();
		Vector<FeatureValue> bagOfWordMapLine = null;

		for(String line : ret) {
			String lineToken[] = line.split("# ");
			String newLine = lineToken[0];
			String key = lineToken[1];
			//System.out.println("[addBagOfWordFeature] key: " +key);
			
			String[] lineInfo = key.split("_");
			//System.out.println("[addBagOfWordFeature] imageId key: " +lineInfo[0]);
			if(textFeature.getFeatureType().equals(FeatureType.LSABoN) || textFeature.getFeatureType().equals(FeatureType.nLSABoN)) {
				bagOfWordMapLine = LSABagOfNMap.get(lineInfo[0]);
			} else if(textFeature.getFeatureType().equals(FeatureType.LSABoNA)) {
				bagOfWordMapLine = LSABagOfNAMap.get(lineInfo[0]);
			} else {
				bagOfWordMapLine = LSABagOfNAVMap.get(lineInfo[0]);
			}

			for(FeatureValue featureValue : bagOfWordMapLine) {
				newLine += featureValue.getPos()+dimension+":"+featureValue.getValue() +" ";
				//System.out.println(" " +newFeatureValue.getPos() +" " +newFeatureValue.getValue());
			}
			//System.out.println("[addBagOfWordFeature] key: " +key +" dopo: " +combinationLine.size());
			newLine += "# " +key;
			ret.set(ret.indexOf(line), newLine);
		}

		return ret;

	}

	/**
	 * Carica la rappresentazione del tipo specificato in feature, dei vettori di feature che rappresentano le didascalie delle immagini.
	 * @param feature
	 * @return
	 */
	public HashMap<String, Vector<FeatureValue>> loadBagOfWordFeatureRepresentation(Feature feature) {
		HashMap<String, Vector<FeatureValue>> featureValuesMap = new HashMap<String, Vector<FeatureValue>>();

		FileReader fileReader;
		String line;
		try {
			String filePath = Main.datasetAbsolutePath +"textFeature/" +Main.phase +"/" +feature.featurePath();
			fileReader = new FileReader(filePath);

			BufferedReader buffReader = new BufferedReader(fileReader);

			/// Leggo la prima linea del file dove sono contenuti i commenti
			line = buffReader.readLine();
			String[] lineToken = line.split(" ");
			// Prendo il numero di feature
			int sizeFeature = Integer.valueOf(lineToken[2]);	
			((TextFeature) feature).setDictionarySize(sizeFeature);
			System.out.println("[loadBagOfWordFeatureRepresentation] sizeFeature " +sizeFeature +" for " +feature.getFeatureType());

			while(true) {
				line = buffReader.readLine();
				if(line == null)
					break;
				FeatureLine textFeatureLine = textFeatureLineSplit(line);
				featureValuesMap.put(textFeatureLine.getImageId(), textFeatureLine.getFeatureValues());
			}
			buffReader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return featureValuesMap;
	}

	/**
	 * Splitta una linea di testo che rappresenta un vettore di feature per una textFeature.
	 * @param line la linea di testo da parsare
	 * @return Ritorna un oggetto di tipo FeatureLine che contiene l'id dell'immagine a cui si riferisce e il vettore con le feature
	 */
	public FeatureLine textFeatureLineSplit(String line) {
		Vector<FeatureValue> featureValues = new Vector<FeatureValue>();

		String[] lineToken = line.split("#");
		/// Nel primo token si trovano i valori delle feature
		String newLine = lineToken[0];
		/// Nel secondo si trovano le info per quella linea (imageId)
		String info = lineToken[1];
		String[] infoToken = info.split(" ");

		String[] featureString = newLine.split(" ");

		/// Parto dal secondo elemento, il primo è il +1
		for(int idx = 1; idx < featureString.length; idx++) {
			String[] featureToken = featureString[idx].split(":");

			//System.out.println("Pos: " +featureToken[0] +" Value: " +featureToken[1]);
			featureValues.add(new FeatureValue(new Integer(featureToken[0]), new BigDecimal(featureToken[1])));
		}

		return new FeatureLine(infoToken[1], featureValues);

	}

	
	public void createDatasetFileOnlyFornLSABoN(String instancesFilesDir) {
		/// Carico le informazioni del nLSABoN
		System.out.println("Lettura feature nLSABoN...");
		Feature featurenLSABoN = new FeaturenLSABoN();
		HashMap<String, Vector<FeatureValue>> nLSABagOfNMap = loadBagOfWordFeatureRepresentation(featurenLSABoN);

		/// Mantiene la directory dove scrivere i file delle categorie
		String categoryFilesDir = Main.datasetAbsolutePath +"combinations/" +Main.phase +"/" +featurenLSABoN.getDirNamePart() +"/128_96/";
		/// Creo la cartella se non esiste
		File categoryFilesDirFile = new File(categoryFilesDir);
		categoryFilesDirFile.mkdirs();
		
		/// mantiene le featurenLSABoN per la corrente immagine
		Vector<FeatureValue> nLSABoNForCurrentImage = null;

		/// Leggo il file che contiene le categorie
		readCategoryIds();
		/// Creo un file per ogni categoria dove nella prima linea ci sono le informazioni
		for(String categoryId : categoryIds) {
			try {
				BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(categoryFilesDir +categoryId +".dat"));
				bufferedWriter.write("# " +featurenLSABoN.getInfo() +"\n");
				bufferedWriter.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		/// Prendo i descrittori dei file nella cartella, il nome di ogni file è l'imageId all'interno di ogni file ogni riga 
		/// è nella forma instanceId_catId
		File instancesFiles[] = FileManager.getFiles(instancesFilesDir);
		/// Per ogni file
		for(File instancesFile : instancesFiles) {
			/// Prendo il suo nome che è l'imageId
			String imageId = instancesFile.getName();

			/// Prendo le feature nLSABoN riferite all'imageId
			nLSABoNForCurrentImage = nLSABagOfNMap.get(imageId);

			String instanceId, categoryId;
			String line, lineToken[];
			String featureLine;

			Vector<String> prevCategoryId = new Vector<String>();

			try {
				BufferedReader buffReader = new BufferedReader(new FileReader(instancesFile));
				while ((line = buffReader.readLine()) != null) {
					/// Prendo instance e category id  
					lineToken = line.split("_");
					instanceId = lineToken[0];
					categoryId = lineToken[1];
					/// Se non ho già scritto questo nLSABoN per questa categoria lo inserisco nel file della categoria
					if(!prevCategoryId.contains(categoryId)) {
						prevCategoryId.add(categoryId); /// Inserisco il categoryId nel vettore per informare che quella categoria è già stata vista
						/// Costruisco la linea con le feature da scrivere nel file della categoria
						featureLine = "+1 ";
						for(FeatureValue featureValue : nLSABoNForCurrentImage) {
							featureLine += featureValue.getPos() +":" +featureValue.getValue().toString() +" ";
						}
						featureLine += "# " +imageId +" " +instanceId;
						/// Scrivo la linea nel file della categoria
						BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(categoryFilesDir +categoryId +".dat", true));
						bufferedWriter.write(featureLine +"\n");
						bufferedWriter.close();
						
					}
				}
				buffReader.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	public void addFeatureAtCombination(String pathToCombinationDir, String featureInCombination, String featureToAdd, String dimension, String featureToAddDirPath) {
		HashMap<String, Vector<FeatureValue>> featureToAddForCategory;
		
		/// Creo la directory dove inserire i nuovi file
		String featureInCombinationToken[] = featureInCombination.split("_");
		String newCombinationDirPath = Main.datasetAbsolutePath +featureInCombination+featureToAdd +"/" +dimension +"/";
		
		readCategoryIds();
		
		/// Leggo la prima linea di uno dei file per conoscere il numero di feature presenti nei file
		String firstLineToken[] = null;
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(pathToCombinationDir +"/" +categoryIds.firstElement() +".dat"));
			firstLineToken = bufferedReader.readLine().split(" ");
			bufferedReader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String totalNumOfFeature = firstLineToken[2];
		
		/// Per ogni categoria apro il file e lo leggo linea per linea
		for(String categoryId : categoryIds) {
			featureToAddForCategory = new HashMap<String, Vector<FeatureValue>>();
			try {
				String featureLine, featureLineToken[];
				String info, infoToken[];
				/// Apro i diversi file
				BufferedReader combinationReader = new BufferedReader(new FileReader(pathToCombinationDir +"/" +categoryIds.firstElement() +".dat"));
				BufferedReader featureToAddReader = new BufferedReader(new FileReader(featureToAddDirPath +"/" +categoryIds.firstElement() +".dat"));
				BufferedWriter newCombinationWriter = new BufferedWriter(new FileWriter(newCombinationDirPath +"/" +categoryIds.firstElement() +".dat"));
				/// Costruisco la prima linea di commento e la scrivo sul nuovo file
				info = combinationReader.readLine(); /// Leggo la prima linea che è un commento
				String infoToAdd = featureToAddReader.readLine();
				String infoToAddToken[] = infoToAdd.split("# ");
				newCombinationWriter.write(info +" " +infoToAddToken[1]);
				/// Leggo tutto il file con le feature da aggiungere
				while ((featureLine = featureToAddReader.readLine()) != null) {
					/// Splitto linea e la salvo in una HashMap
					FeatureLine featureLineSplit = featureLineSplit(featureLine);
					featureToAddForCategory.put(featureLineSplit.getInstanceId(), featureLineSplit.getFeatureValues());
				}
				featureToAddReader.close();
				
				while ((featureLine = combinationReader.readLine()) != null) {
					/// Splitto rispetto al commento della linea, cosi nella prima parte ho solo il vettore delle feature
					featureLineToken = featureLine.split("# ");
					featureLine = featureLineToken[0];
					infoToken = featureLineToken[1].split(" ");
					String instanceId = infoToken[1];
					
					/// Ad ogni linea aggiungo la feature e salvo la linea nel nuovo file
					Vector<FeatureValue> featureValuesToAdd = featureToAddForCategory.get(instanceId);
					for(FeatureValue featureValue : featureValuesToAdd) {
						featureLine += featureValue.getPos()+totalNumOfFeature +":" +featureValue.getValue();
					}
					
				}
				
				combinationReader.close();
				newCombinationWriter.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
				
	}

}
